function countLetter(letter, sentence) {
    let result = 0;
    let count;
   
    for(let i = count = 0; i < sentence.length; count +=+ (letter === sentence[i++])){
        count;
    }

    if(count != 0){
        return result = count;
    }
    // Check first whether the letter is a single character.
    // If letter is a single character, count how many times a letter has occurred in a given sentence then return count.
    // If letter is invalid, return undefined.
}


function isIsogram(text) {

    // An isogram is a word where there are no repeating letters.
    // The function should disregard text casing before doing anything else.
    // If the function finds a repeating letter, return false. Otherwise, return true.
    text = text.toLowerCase();
    for(let i = 0; i < text.length; i++){
        for(let n = i + 1; n < text.length; n++){
            if(text[i] === text[n]){
                return false;
            }
        }
    }
    return true;
}

function purchase(age, price) {
    let result;
    const discountedPrice = price * 0.8;
    const roundedPrice = price.toFixed(2);
   
    if ((age >= 13 && age <= 21) || (age > 64)){
        return result = discountedPrice.toFixed(2);
    } else if ((age > 22) && (age <= 64)){
        return result = roundedPrice;
    } else{
        return result;
    };

     // Return undefined for people aged below 13.
    // Return the discounted price (rounded off) for students aged 13 to 21 and senior citizens. (20% discount)
    // Return the rounded off price for people aged 22 to 64.
    // The returned value should be a string.
}

function findHotCategories(items) {
   let filteredArray = items.filter((item) => {
    if(item.stocks === 0) return item;
   })

   let result = [];
   filteredArray.forEach((filtered, i) => {
    let category = filteredArray[i].category;
    if(!result.includes(category)){
        result.push(category);
    }
   })   

   return result;

    // Find categories that has no more stocks.
    // The hot categories must be unique; no repeating categories.

    // The passed items array from the test are the following:
    // { id: 'tltry001', name: 'soap', stocks: 14, category: 'toiletries' }
    // { id: 'tltry002', name: 'shampoo', stocks: 8, category: 'toiletries' }
    // { id: 'tltry003', name: 'tissues', stocks: 0, category: 'toiletries' }
    // { id: 'gdgt001', name: 'phone', stocks: 0, category: 'gadgets' }
    // { id: 'gdgt002', name: 'monitor', stocks: 0, category: 'gadgets' }

    // The expected output after processing the items array is ['toiletries', 'gadgets'].
    // Only putting return ['toiletries', 'gadgets'] will not be counted as a passing test during manual checking of codes.

}

function findFlyingVoters(candidateA, candidateB) {
    let results = [];

    for(let i = 0; i < candidateA.length; i++){
        for(let n = 0; n < candidateB.length; n++){
            if (candidateA[i] === candidateB[n]) results.push(candidateA[i]);
        }
    }

    return results;
    // Find voters who voted for both candidate A and candidate B.

    // The passed values from the test are the following:
    // candidateA: ['LIWf1l', 'V2hjZH', 'rDmZns', 'PvaRBI', 'i7Xw6C', 'NPhm2m']
    // candidateB: ['kcUtuu', 'LLeUTl', 'r04Zsl', '84EqYo', 'V2hjZH', 'LIWf1l']

    // The expected output after processing the candidates array is ['LIWf1l', 'V2hjZH'].
    // Only putting return ['LIWf1l', 'V2hjZH'] will not be counted as a passing test during manual checking of codes.
    
}

module.exports = {
    countLetter,
    isIsogram,
    purchase,
    findHotCategories,
    findFlyingVoters
};